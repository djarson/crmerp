var Cliente_llamada=require('../models/Cliente_llamada')

const crear_llamada_prospeccion_admin=async function(req,res){
    if(req.user){//valida la cabecera de pospeccion
        let data=req.body;
        let llamada=await Cliente_llamada.create(data);
        res.status(200).send({data: llamada})
    }else{
        res.status(403).send({data:undefined,message:'NoToken'});
    }
}

const listar_llamadas_prospeccion_admin=async function(req, res) {  
    if(req.user){//valida la cabecera de pospeccion
        let id = req.params['id'];
        let llamadas = await Cliente_llamada.find({cliente:id}).populate('asesor').sort({createdAT:-1})
        res.status(200).send({data:llamadas})
        
    }else{
        res.status(403).send({data:undefined,message:'NoToken'});
    }
    
}

module.exports={
    crear_llamada_prospeccion_admin,
    listar_llamadas_prospeccion_admin
}