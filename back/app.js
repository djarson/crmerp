var express = require('express');
var mongoose = require('mongoose');
var port = process.env.PORT || 4201;

var bodyparser = require('body-parser');

var app = express();
var test_routes = require('./routes/test')
var colaborador_routes = require('./routes/colaborador')
var cliente_routes = require('./routes/cliente')
var prospeccion_routes = require('./routes/prospeccion')


mongoose.connect('mongodb://127.0.0.1:27017/negocio', { useUnifiedTopology: true, useNewUrlParser: true }, (err, res) => {
        if (err) {
            console.log(err);
        } else {
            console.log('servidor corriendo en el puerto');
            app.listen(port, function() {
                console.log("en el puerto" + ' ' + port)
            })
        }
    })
    ///////para parcear la  informacio del front deservidor externo
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json({ limit: '50mb', extended: true }));

app.use((req, res, next) => { //esta configuracionme permite recibir informacion de cualquier dominio y permitir la actualizaciones en las peticiones del front al back
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Allow', 'GET, PUT, POST, DELETE, OPTIONS');
    next();
});

app.use('/api', test_routes);
app.use('/api', colaborador_routes);
app.use('/api', cliente_routes);
app.use('/api', prospeccion_routes);

module.exports = app;