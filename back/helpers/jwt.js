var jwt = require('jwt-simple')
var momento = require('moment')
var secret = 'djarson'

exports.createToken = function(user) {
    var payload = {
        sub: user._id,
        nombres: user.nombres,
        apellidos: user.apellidos,
        email: user.email,
        rol: user.rol,
        iat: momento().unix,
        exp: momento().add(7, 'days').unix(),
    }
    return jwt.encode(payload, secret);
}