var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Cliente_interesSchema = Schema({
    fecha: { type: 'string', required: true },
    tipo: { type: 'string', required: true },
    nivel: { type: 'string', required: true },
    nota: { type: 'string', required: true },
    ciclo: { type: 'string', required: true },



    asesor: { type: Schema.ObjectId, ref: 'colaborador', require: false },
    cliente: { type: Schema.ObjectId, ref: 'cliente', require: false },
    curso: { type: Schema.ObjectId, ref: 'curso', require: false },

    createdAT: { type: 'Date', default: Date.now, required: true },

   

});

module.exports = mongoose.model('cliente_interes', Cliente_interesSchema);