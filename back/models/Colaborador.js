var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ColaboradorSchema = Schema({
    nombres: { type: 'string', required: true },
    apellidos: { type: 'string', required: true },
    email: { type: 'string', required: true },
    fullnames: { type: 'string', required: true },
    telefono: { type: 'string', required: false },
    rol: { type: 'string', required: true },
    genero: { type: 'string', required: true },
    n_doc: { type: 'string', required: false },
    password: { type: 'string', required: true },
    estado: { type: 'Boolean', default: true, required: true },
    pais: { type: 'string', required: false },
    createdAT: { type: 'Date', default: Date.now, required: true },

});

module.exports = mongoose.model('colaborador', ColaboradorSchema);