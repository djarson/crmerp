var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClienteSchema = Schema({
    nombres: { type: 'string', required: true },
    apellidos: { type: 'string', required: true },
    email: { type: 'string', required: true },
    fullnames: { type: 'string', required: true },
    telefono: { type: 'string', required: true },
    nacimiento: { type: 'string', required: false },
    genero: { type: 'string', required: true },
    verify: { type: 'Boolean', default: false, required: true },
    n_doc: { type: 'string', required: false },
    tipo: { type: 'string', default: 'Prospecto', required: true },
    password: { type: 'string', required: true },
    estado: { type: 'Boolean', default: true, required: true },
    pais: { type: 'string', required: false },
    ciudad: { type: 'string', required: false },
    asesor: { type: Schema.ObjectId, ref: 'colaborador', require: false },
    createdAT: { type: 'Date', default: Date.now, required: true },

});

module.exports = mongoose.model('cliente', ClienteSchema);