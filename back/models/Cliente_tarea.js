var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Cliente_tareaSchema = Schema({
    fecha: { type: 'string', required: true },
    nota: { type: 'string', required: true },
    tipo: { type: 'string', required: true },
    actividad: { type: Boolean, required: true },
    asesor: { type: Schema.ObjectId, ref: 'colaborador', require: false },
    cliente: { type: Schema.ObjectId, ref: 'cliente', require: false },
    marca_asesor: { type: Schema.ObjectId, ref: 'colaborador', require: false },
    createdAT: { type: 'Date', default: Date.now, required: true },

   

});

module.exports = mongoose.model('cliente_tarea', Cliente_tareaSchema);