var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Cliente_llamadaSchema = Schema({
    fecha: { type: 'string', required: true },
    hora: { type: 'string', required: true },
    resultado: { type: 'string', required: true },
    nota: { type: 'string', required: false },
    asesor: { type: Schema.ObjectId, ref: 'colaborador', require: false },
    cliente: { type: Schema.ObjectId, ref: 'cliente', require: false },
    createdAT: { type: 'Date', default: Date.now, required: true },

   

});

module.exports = mongoose.model('cliente_llamada', Cliente_llamadaSchema);