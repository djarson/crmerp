var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Cliente_correoSchema = Schema({
    asunto: { type: 'string', required: true },
    contenido: { type: 'string', required: true },
    asesor: { type: Schema.ObjectId, ref: 'colaborador', require: false },
    cliente: { type: Schema.ObjectId, ref: 'cliente', require: false },
    createdAT: { type: 'Date', default: Date.now, required: true },

   

});

module.exports = mongoose.model('cliente_correo', Cliente_correoSchema);