var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Cliente_actividadSchema = Schema({
    tipo: { type: 'string', required: true },
    actividad: { type: 'string', required: true },
    asesor: { type: Schema.ObjectId, ref: 'colaborador', require: false },
    cliente: { type: Schema.ObjectId, ref: 'cliente', require: false },
    createdAT: { type: 'Date', default: Date.now, required: true },

   

});

module.exports = mongoose.model('cliente_actividad', Cliente_actividadSchema);