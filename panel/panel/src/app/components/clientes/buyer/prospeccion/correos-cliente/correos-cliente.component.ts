import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-correos-cliente',
  templateUrl: './correos-cliente.component.html',
  styleUrls: ['./correos-cliente.component.css']
})
export class CorreosClienteComponent implements OnInit {

  public id:any=''

  constructor(
    private _route:ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._route.params.subscribe(
      params=>{
        this.id=params['id']
       
      }
    );
  }

}